import {createTheme, ThemeProvider} from '@mui/material/styles';

const fonts = createTheme({
  typography: {
    fontFamily: [
      'cmu',
    ].join(','),
  },
});

const theme = createTheme({
  typography: {
    fontFamily: [
      'cmu',
    ].join(','),
  },
	text: {
		secondary: '#888888',
	},
  palette: {
    type: 'light',
    primary: {
      main: '#d28712',
      light: '#4caf50',
      contrastText: '#f6f5c9',
      dark: '#634225',
    },
    secondary: {
      main: '#b71c1c',
      light: '#f7c870',
      dark: '#560c0c',
    },
  },
});

export default theme;
