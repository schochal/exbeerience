<?php

namespace App\Controller;

use App\Entity\Beer;
use App\Repository\BeerRepository;
use App\Repository\BreweryRepository;

use Doctrine\Persistence\ManagerRegistry;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class EditBeerController extends AbstractController
{
    #[Route('/edit/beer/{id}', name: 'edit_beer')]
    public function index(int $id, BeerRepository $br): Response
    {
        if($id == 0) {
            $title = 'Add Beer';
        } else {
            $beer = $br->findOneById($id);
            $title = 'Edit ' . $beer->getName();
        }
        
        return $this->render('edit_beer/index.html.twig', [
            'beer' => $title,
        ]);
    }

    //#[Route('/edit/beer', name: 'new_beer')]
    //public function new(beerRepository $br): Response
    //{
    //    return $this->index(0, $br);
    //}

    #[Route('/api/edit/beer/{id}', name: 'edit_beer_api')]
    public function edit_beer_api(int $id, BeerRepository $br): Response
    {
        if($id == 0) {
            $beer = new Beer();
            $beer->setName('');
            $beer->setType('');
            $beer->setDescription('');
            $beer->setAlc('');
            $beer->setSlug('');
            $beer->setPrice(0);
            $beer->setIsTapped(false);
        } else {
            $beer = $br->findOneById($id);
        }
        
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $json = $serializer->normalize($beer, 'json', [
          'circular_reference_handler' => function ($object) {
            return $object->getId();
          }
        ]);

        return new JsonResponse($json);
    }

    #[Route('/api/edit/update-beer/{id}', name: 'update_beer_api')]
    public function update_beer_api(int $id, ManagerRegistry $doctrine, BeerRepository $br, Request $request, BreweryRepository $brr): Response
    {
        $data = $request->getContent();
        $data = json_decode($data);

        if($id == 0) {
            $beer = new Beer();
        } else {
            $beer = $br->findOneById($id);
        } 

				$brewery = $brr->findOneById($data->brewery);

        $beer->setName($data->beer->Name);
        $beer->setType($data->beer->Type);
        $beer->setSlug($data->beer->Slug);
        $beer->setAlc($data->beer->Alc);
        $beer->setPrice($data->beer->Price);
        $beer->setBrewery($brewery);
        $beer->setDescription($data->beer->Description);
        $beer->setIsTapped($data->beer->isTapped);

        $em = $doctrine->getManager();
        $em->persist($beer);
        $em->flush($beer);

        return $this->json([
          'status' => 'OK',
          'message' => 'OK',
        ]);
    }
}
