import React from 'react';
import * as ReactDOM from 'react-dom/client';

import {
  Alert,
  Button,
  Checkbox,
  Container,
  FormControlLabel,
  Grid,
  Snackbar,
  TextField,
  Typography,
} from '@mui/material';

import { ThemeProvider } from '@mui/material/styles';

import theme from './theme';

import axios from 'axios';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
			username: '',
			password: '',
      snackbar: false,
      snackbarError: false,
			invalidCredentials: false,
			message: '',
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
		e.preventDefault();
    if(this.state.username != '' && this.state.password != '') {
			axios.post('/api/login', {
				username: this.state.username,
				password: this.state.password,
			}).then((response) => {
				if(response.data.status != 'OK') {
					this.setState({snackbarError: true, message: response.data.message});
					return;
				} else {
					window.location = '/';
				}
			});
    } else {
      this.setState({snackbarError: true, message: 'please enter a username and a password'});
    }
  }

  render() {
    return (
      <ThemeProvider theme={theme}>
				<Container>
					<Alert severity="info" style={{ marginTop: '20px' }}>
						This website places a cookie which keeps you logged in into your browser. It will be deleted after one week.
					</Alert>
					<Typography variant="h4" style={{ marginTop: '20px' }}>Login</Typography>
						<form>
						<Grid container spacing={2} style={{ marginTop: '20px' }}>
							{this.state.invalidCredentials && 
							<Grid item xs={12}>
								<Alert severity="error">Username or Password incorrect</Alert>
							</Grid> }
							<Grid item xs={12} md={6}>
								<TextField
									label="Username"
									variant="outlined"
									value={this.state.username}
									onChange={(e) => this.setState({username: e.target.value})}
									fullWidth
								/>
							</Grid>
							<Grid item xs={12} md={6}>
								<TextField
									label="Password"
									variant="outlined"
									value={this.state.password}
									type="password"
									onChange={(e) => this.setState({password: e.target.value})}
									fullWidth
								/>
							</Grid>
							<Grid item xs={12}>
								<Button
									type="submit"
									variant="contained"
									onClick={(e) => {this.handleSubmit(e)}}
								>
									Submit
								</Button>
							</Grid>
						</Grid>
					</form>
					<Snackbar open={this.state.snackbarError} autoHideDuration={6000} onClose={() => {this.setState({snackbarError: false});}}>
						<Alert severity="error" sx={{ width: '100%' }}>
							{this.state.message}
						</Alert>
					</Snackbar>
				</Container>
			</ThemeProvider>
    )
  }
}

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
			username: '',
			password: '',
			password2: '',
      snackbar: false,
      snackbarError: false,
			message: '',
			first_name: '',
			last_name: '',
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
		e.preventDefault();
    if(this.state.username == '' || this.state.password == '' || this.state.password2 == '' || this.state.first_name == '' || this.state.last_name == '') {
      this.setState({snackbarError: true, message: 'please fill all fields'});
			return;
		}
    if(this.state.password != this.state.password2) {
      this.setState({snackbarError: true, message: 'the passwords do not match'});
			this.setState({password: '', password2: ''})
			return;
		}
		axios.post('/api/register', {
			username: this.state.username,
			password: this.state.password,
			first_name: this.state.first_name,
			last_name: this.state.last_name,
		}).then((response) => {
				if(response.data.status != 'OK') {
					this.setState({snackbarError: true, message: response.data.message});
					return;
				}
				axios.post('/api/login', {
					username: this.state.username,
					password: this.state.password,
				}).then(() => {
					window.location = '/';
				});
			}
		);
  }

  render() {
    return (
      <ThemeProvider theme={theme}>
				<Container>
					<Typography variant="h4" style={{ marginTop: '50px' }}>Register</Typography>
						<form>
						<Grid container spacing={2} style={{ marginTop: '20px' }}>
							{this.state.invalidCredentials && 
							<Grid item xs={12}>
								<Alert severity="error">Username or Password incorrect</Alert>
							</Grid> }
							<Grid item xs={12}>
								<TextField
									label="Username"
									variant="outlined"
									value={this.state.username}
									onChange={(e) => this.setState({username: e.target.value})}
									fullWidth
								/>
							</Grid>
							<Grid item xs={12} md={6}>
								<TextField
									label="First Name"
									variant="outlined"
									value={this.state.first_name}
									onChange={(e) => this.setState({first_name: e.target.value})}
									fullWidth
								/>
							</Grid>
							<Grid item xs={12} md={6}>
								<TextField
									label="Last Name"
									variant="outlined"
									value={this.state.last_name}
									onChange={(e) => this.setState({last_name: e.target.value})}
									fullWidth
								/>
							</Grid>
							<Grid item xs={12} md={6}>
								<TextField
									label="Password"
									variant="outlined"
									value={this.state.password}
									type="password"
									onChange={(e) => this.setState({password: e.target.value})}
									fullWidth
								/>
							</Grid>
							<Grid item xs={12} md={6}>
								<TextField
									label="repeat Password"
									variant="outlined"
									value={this.state.password2}
									type="password"
									onChange={(e) => this.setState({password2: e.target.value})}
									fullWidth
								/>
							</Grid>
							<Grid item xs={12}>
								<Button
									type="submit"
									variant="contained"
									onClick={(e) => {this.handleSubmit(e)}}
								>
									Register
								</Button>
							</Grid>
						</Grid>
					</form>
					<Snackbar open={this.state.snackbarError} autoHideDuration={6000} onClose={() => {this.setState({snackbarError: false});}}>
						<Alert severity="error" sx={{ width: '100%' }}>
							{this.state.message}
						</Alert>
					</Snackbar>
				</Container>
			</ThemeProvider>
    )
  }
}

const container = document.getElementById('login');
const root = ReactDOM.createRoot(container);
root.render(<Login/>);
const containerR = document.getElementById('register');
const rootR = ReactDOM.createRoot(containerR);
rootR.render(<Register/>);
