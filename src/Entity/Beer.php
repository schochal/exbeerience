<?php

namespace App\Entity;

use App\Repository\BeerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BeerRepository::class)]
class Beer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $Name;

    #[ORM\ManyToOne(targetEntity: Brewery::class, inversedBy: 'beers')]
    private $Brewery;

    #[ORM\Column(type: 'string', length: 255)]
    private $Type;

    #[ORM\Column(type: 'text', nullable: true)]
    private $Description;

    #[ORM\Column(type: 'string', length: 255)]
    private $Alc;

    #[ORM\Column(type: 'string', length: 255)]
    private $Slug;

    #[ORM\OneToMany(mappedBy: 'Beer', targetEntity: Comment::class)]
    private $comments;

    #[ORM\OneToMany(mappedBy: 'Beer', targetEntity: Flavour::class, orphanRemoval: true)]
    private $flavours;

    #[ORM\Column(type: 'float')]
    private $Price;

    #[ORM\Column(type: 'boolean')]
    private $isTapped;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->flavours = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    public function getBrewery(): ?Brewery
    {
        return $this->Brewery;
    }

    public function setBrewery(?Brewery $Brewery): self
    {
        $this->Brewery = $Brewery;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->Type;
    }

    public function setType(string $Type): self
    {
        $this->Type = $Type;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(?string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getAlc(): ?string
    {
        return $this->Alc;
    }

    public function setAlc(string $Alc): self
    {
        $this->Alc = $Alc;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->Slug;
    }

    public function setSlug(string $Slug): self
    {
        $this->Slug = $Slug;

        return $this;
    }

    /**
     * @return Collection<int, Comment>
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setBeer($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getBeer() === $this) {
                $comment->setBeer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Flavour>
     */
    public function getFlavours(): Collection
    {
        return $this->flavours;
    }

    public function addFlavour(Flavour $flavour): self
    {
        if (!$this->flavours->contains($flavour)) {
            $this->flavours[] = $flavour;
            $flavour->setBeer($this);
        }

        return $this;
    }

    public function removeFlavour(Flavour $flavour): self
    {
        if ($this->flavours->removeElement($flavour)) {
            // set the owning side to null (unless already changed)
            if ($flavour->getBeer() === $this) {
                $flavour->setBeer(null);
            }
        }

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->Price;
    }

    public function setPrice(float $Price): self
    {
        $this->Price = $Price;

        return $this;
    }

    public function getIsTapped(): ?bool
    {
        return $this->isTapped;
    }

    public function setIsTapped(bool $isTapped): self
    {
        $this->isTapped = $isTapped;

        return $this;
    }
}
