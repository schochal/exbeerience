<?php

namespace App\Entity;

use App\Repository\CommentRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CommentRepository::class)]
class Comment
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'text')]
    private $Comment;

    #[ORM\Column(type: 'float')]
    private $Rating;

    #[ORM\ManyToOne(targetEntity: Beer::class, inversedBy: 'comments')]
    private $Beer;

    #[ORM\Column(type: 'string', length: 255)]
    private $Name;

    #[ORM\Column(type: 'string', length: 255)]
    private $Timestamp;

    #[ORM\Column(type: 'string', length: 255)]
    private $TimestampSort;

    #[ORM\ManyToOne(targetEntity: User::class)]
    private $Author;

    #[ORM\Column(type: 'boolean')]
    private $isPublic;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getComment(): ?string
    {
        return $this->Comment;
    }

    public function setComment(string $Comment): self
    {
        $this->Comment = $Comment;

        return $this;
    }

    public function getRating(): ?float
    {
        return $this->Rating;
    }

    public function setRating(float $Rating): self
    {
        $this->Rating = $Rating;

        return $this;
    }

    public function getBeer(): ?Beer
    {
        return $this->Beer;
    }

    public function setBeer(?Beer $Beer): self
    {
        $this->Beer = $Beer;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    public function getTimestamp(): ?string
    {
        return $this->Timestamp;
    }

    public function setTimestamp(string $Timestamp): self
    {
        $this->Timestamp = $Timestamp;

        return $this;
    }

    public function getTimestampSort(): ?string
    {
        return $this->TimestampSort;
    }

    public function setTimestampSort(string $TimestampSort): self
    {
        $this->TimestampSort = $TimestampSort;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->Author;
    }

    public function setAuthor(?User $Author): self
    {
        $this->Author = $Author;

        return $this;
    }

    public function getIsPublic(): ?bool
    {
        return $this->isPublic;
    }

    public function setIsPublic(bool $isPublic): self
    {
        $this->isPublic = $isPublic;

        return $this;
    }
}
