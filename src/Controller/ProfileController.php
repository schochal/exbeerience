<?php

namespace App\Controller;

use App\Repository\CommentRepository;
use App\Repository\FlavourRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ProfileController extends AbstractController
{
    #[Route('/profile', name: 'profile')]
    public function index(): Response
    {
        return $this->render('profile/index.html.twig');
    }

    #[Route('/api/profile', name: 'profile_api')]
    public function profile_api(CommentRepository $cr, FlavourRepository $fr): Response
    {
        $user = $this->getUser();
        $comments = $cr->findByUser($user);

        $beers = array();
        $no_duplicates = array();

        foreach($comments as $comment) {
          if(!in_array($comment->getBeer()->getId(), $no_duplicates)) {
            $beer = $comment->getBeer();
            $comments = $cr->findByBeerAndUser($beer, $user);
						$flavours = $fr->findByBeerAndUser($beer, $user);
            $obj = array(
              'beer' => $beer,
              'comments' => $comments,
							'flavours' => $flavours,
            );
            array_push(
              $beers,
              $obj,
            );
            array_push(
              $no_duplicates,
              $beer->getId()
            );
          }
        }

        $flavours = $fr->findByUser($user);
       	foreach($flavours as $flavour) {
          if(!in_array($flavour->getBeer()->getId(), $no_duplicates)) {
            $beer = $flavour->getBeer();
            $comments = $cr->findByBeerAndUser($beer, $user);
						$flavours = $fr->findByBeerAndUser($beer, $user);
            $obj = array(
              'beer' => $beer,
              'comments' => $comments,
							'flavours' => $flavours,
            );
            array_push(
              $beers,
              $obj,
            );
            array_push(
              $no_duplicates,
              $beer->getId()
            );
          }
        }

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $json = $serializer->normalize($beers, 'json', [
          'circular_reference_handler' => function ($object) {
            return $object->getId();
          }
        ]);

        return new JsonResponse($json);
    }
}
