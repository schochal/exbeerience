<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220430182933 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE flavour ADD beer_id INT NOT NULL');
        $this->addSql('ALTER TABLE flavour ADD CONSTRAINT FK_4FF2D983D0989053 FOREIGN KEY (beer_id) REFERENCES beer (id)');
        $this->addSql('CREATE INDEX IDX_4FF2D983D0989053 ON flavour (beer_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE flavour DROP FOREIGN KEY FK_4FF2D983D0989053');
        $this->addSql('DROP INDEX IDX_4FF2D983D0989053 ON flavour');
        $this->addSql('ALTER TABLE flavour DROP beer_id');
    }
}
