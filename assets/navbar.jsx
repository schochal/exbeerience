import React from 'react';
import ReactDOM from 'react-dom/client';


import {
	AppBar,
	Box,
	Button,
	Container,
	Divider,
	Drawer,
	IconButton,
	List,
	ListItem,
	ListItemIcon,
	ListItemText,
	Menu,
	MenuItem,
	Toolbar,
	Tooltip,
	Typography,
} from '@mui/material';

import HomeIcon from '@mui/icons-material/Home';
import LoginIcon from '@mui/icons-material/Login';
import PasswordIcon from '@mui/icons-material/Password';
import LogoutIcon from '@mui/icons-material/Logout';
import MenuIcon from '@mui/icons-material/Menu';
import SportsBarIcon from '@mui/icons-material/SportsBar';

import {ThemeProvider} from '@mui/material/styles';
import { styled } from '@mui/material/styles';
import theme from './theme';

class Navbar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      drawer: false,
			user: {},
    };

    this.handleOpenNavMenu = this.handleOpenNavMenu.bind(this);
    this.handleCloseNavMenu = this.handleCloseNavMenu.bind(this);
		this.getUser = this.getUser.bind(this);
		this.isLoggedIn = this.isLoggedIn.bind(this);
  }

	componentDidMount() {
		this.getUser();
	}

  handleOpenNavMenu = () => {
    this.setState({drawer: true});
  };

  handleCloseNavMenu = () => {
    this.setState({drawer: false});
  };

	isLoggedIn() {
		if(Object.entries(this.state.user).length == 0)
			return false;
		return true;
	}

	async getUser() {
    const response = await fetch('/api/user');
    const data = await response.json();
    this.setState({
      user: data,
		});
	}

	render () {
    return (
      <Box>
      <ThemeProvider theme={theme}>
      <AppBar position="sticky" style={{ backgroundColor: theme.palette.primary.dark }} elevation={0}>
        <Container>
          <Toolbar disableGutters>
            <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
              <IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={this.handleOpenNavMenu}
                color="inherit"
              >
                <MenuIcon />
              </IconButton>
              <Drawer
                anchor='left'
                open={this.state.drawer}
                onClose={this.handleCloseNavMenu}
              >
                <List>
									{this.isLoggedIn() &&
									<ListItem>
										<Typography>
											Logged in as {this.state.user.firstName} {this.state.user.lastName} ({this.state.user.username})
										</Typography>
									</ListItem>
									}
									<ListItem button>
										<ListItemIcon>
											<HomeIcon/>
										</ListItemIcon>
										<ListItemText primary='Main' onClick={() => {window.location = '/';}} />
									</ListItem>
									{!this.isLoggedIn() ?
									<ListItem button>
										<ListItemIcon>
											<LoginIcon/>
										</ListItemIcon>
										<ListItemText primary="Login/Register" onClick={() => {window.location = '/login';}} />
									</ListItem> : 
									<>
									<ListItem button>
										<ListItemIcon>
											<SportsBarIcon/>
										</ListItemIcon>
										<ListItemText primary="My Beers" onClick={() => {window.location = '/profile';}} />
									</ListItem> 
									<Divider variant="middle" style={{ marginTop: '10px', marginBottom: '10px' }}/>
									<ListItem button>
										<ListItemIcon>
											<LogoutIcon/>
										</ListItemIcon>
										<ListItemText primary="Logout" onClick={() => {window.location = '/logout';}} />
									</ListItem> 
									<ListItem button>
										<ListItemIcon>
											<PasswordIcon/>
										</ListItemIcon>
										<ListItemText primary="Change Password" onClick={() => {window.location = '/change_password';}} />
									</ListItem> </>}
                </List>
              </Drawer>
            </Box>
            { 
              // Desktop 
            }
            <a href="/">
              <img src="/logo.svg" style={{ maxWidth: '80px', margin: '10px' }}/>
            </a>
            <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' }, marginLeft: '30px' }}>
							<Button
								onClick={() => {window.location = '/';}}
								sx={{ my: 2, color: 'white', display: 'block' }}
							>
								Main
							</Button>
							{/*
							<Button
								onClick={() => {window.location = '/';}}
								sx={{ my: 2, color: 'white', display: 'block' }}
							>
								Beers
							</Button>
							<Menu
								id="basic-menu"
								anchorEl={anchorEl}
								open={open}
								onClose={handleClose}
								MenuListProps={{
									'aria-labelledby': 'basic-button',
								}}
							>
								<MenuItem onClick={handleClose}>Profile</MenuItem>
							</Menu>
							*/}
							{!this.isLoggedIn() ?
							<Button
								onClick={() => {window.location = '/login';}}
								sx={{ my: 2, color: 'white', display: 'block' }}
							>
								Login/Register
							</Button> :
							<>
							<Button
								onClick={() => {window.location = '/profile';}}
								sx={{ my: 2, color: 'white', display: 'block' }}
							>
								My Beers
							</Button>
							<Button
								onClick={() => {window.location = '/change_password';}}
								sx={{ my: 2, color: 'white', display: 'block' }}
							>
								Change Password
							</Button>
							<Button
								onClick={() => {window.location = '/logout';}}
								sx={{ my: 2, color: 'white', display: 'block' }}
							>
								Logout
							</Button></>
							}
            </Box>
						{ window.innerWidth > 1025 && this.isLoggedIn() &&
							<Typography component="div" style={{ display: 'block' }}>
								Logged in as {this.state.user.firstName} {this.state.user.lastName} ({this.state.user.username})
							</Typography>
						}
          </Toolbar>
        </Container>
      </AppBar>
      </ThemeProvider>
      </Box>
    );
  }
}

const container = document.getElementById('navbar');
const root = ReactDOM.createRoot(container);
root.render(<Navbar/>);
