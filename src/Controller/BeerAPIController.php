<?php

namespace App\Controller;

use App\Repository\BeerRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class BeerAPIController extends AbstractController
{
    #[Route('/api/beers', name: 'beers_api')]
    public function beers_api(BeerRepository $br): JsonResponse
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
 
        $beers = $br->findAllTapped();

        $json = $serializer->normalize($beers, 'json', [
          'circular_reference_handler' => function ($object) {
            return $object->getId();
          }
        ]);

        return new JsonResponse($json);
    }

    #[Route('/api/beers_untapped', name: 'beers_untapped_api')]
    public function beers_untapped_api(BeerRepository $br): JsonResponse
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
 
        $beers = $br->findAllUntapped();

        $json = $serializer->normalize($beers, 'json', [
          'circular_reference_handler' => function ($object) {
            return $object->getId();
          }
        ]);

        return new JsonResponse($json);
    }

    #[Route('/api/beer/{slug}', name: 'beer_api')]
    public function beer_api(string $slug, BeerRepository $br): JsonResponse
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
 
        $beer = $br->findOneBySlug($slug);

        $json = $serializer->normalize($beer, 'json', [
          'circular_reference_handler' => function ($object) {
            return $object->getId();
          }
        ]);

        return new JsonResponse($json);
    }
}
