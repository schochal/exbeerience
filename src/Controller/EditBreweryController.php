<?php

namespace App\Controller;

use App\Entity\Brewery;
use App\Repository\BreweryRepository;

use Doctrine\Persistence\ManagerRegistry;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class EditBreweryController extends AbstractController
{
    #[Route('/edit/brewery/{id}', name: 'edit_brewery')]
    public function index(int $id, BreweryRepository $br): Response
    {
        if($id == 0) {
            $title = 'Add Brewery';
        } else {
            $brewery = $br->findOneById($id);
            $title = 'Edit ' . $brewery->getName();
        }
        
        return $this->render('edit_brewery/index.html.twig', [
            'brewery' => $title,
        ]);
    }

    #[Route('/edit/brewery', name: 'new_brewery')]
    public function new(BreweryRepository $br): Response
    {
        return $this->index(0, $br);
    }

    #[Route('/api/edit/brewery/{id}', name: 'edit_brewery_api')]
    public function edit_brewery_api(int $id, BreweryRepository $br): Response
    {
        if($id == 0) {
            $brewery = new Brewery();
            $brewery->setName('');
            $brewery->setDescription('');
            $brewery->setHomepage('');
            $brewery->setLogo('');
            $brewery->setIsTappedByBrewery(false);
        } else {
            $brewery = $br->findOneById($id);
        }
        
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $json = $serializer->normalize($brewery, 'json', [
          'circular_reference_handler' => function ($object) {
            return $object->getId();
          }
        ]);

        return new JsonResponse($json);
    }

    #[Route('/api/edit/update-brewery-image', name: 'update_brewery_image_api')]
    public function update_brewery_image_api(Request $request): Response
    {
        $file = $request->files->get('file');

        $file->move(
          'logos',
          $file->getClientOriginalName()
        );
        
        return new Response('OK');
    }

    #[Route('/api/edit/update-brewery/{id}', name: 'update_brewery_api')]
    public function update_brewery_api(int $id, ManagerRegistry $doctrine, BreweryRepository $br, Request $request): Response
    {
        $data = $request->getContent();
        $data = json_decode($data);

        if($id == 0) {
            $brewery = new Brewery();
        } else {
            $brewery = $br->findOneById($id);
        } 

        if($data->file !== null) {
            $exp = explode('.', $data->filename);
            $ext = $exp[count($exp)-1];
            $filename = str_replace(array('.', ' '), "", $data->brewery->Name) . '.' . $ext;
            rename('logos/' . $data->filename, 'logos/' . $filename); 
            $brewery->setLogo($filename);
        }

        $brewery->setName($data->brewery->Name);
        $brewery->setDescription($data->brewery->Description);
        $brewery->setHomepage($data->brewery->Homepage);
        $brewery->setIsTappedByBrewery($data->brewery->isTappedByBrewery);

        $em = $doctrine->getManager();
        $em->persist($brewery);
        $em->flush($brewery);

        return $this->json([
          'status' => 'OK',
          'message' => 'OK',
        ]);
    }
}
