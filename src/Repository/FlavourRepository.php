<?php

namespace App\Repository;

use App\Entity\Flavour;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Flavour>
 *
 * @method Flavour|null find($id, $lockMode = null, $lockVersion = null)
 * @method Flavour|null findOneBy(array $criteria, array $orderBy = null)
 * @method Flavour[]    findAll()
 * @method Flavour[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FlavourRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Flavour::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Flavour $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function findByBeer($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.Beer = :val')
            ->setParameter('val', $value)
            ->orderBy('f.Timestamp', 'DESC')
            ->setMaxResults(100)
            ->getQuery()
            ->getResult()
        ;
    }
    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Flavour $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function findByBeerAndUser($beer, $user)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.Beer = :val')
            ->andWhere('c.Author = :val2')
            ->setParameter('val', $beer)
            ->setParameter('val2', $user)
            ->orderBy('c.Timestamp', 'DESC')
            ->setMaxResults(100)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByUser($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.Author = :val')
            ->setParameter('val', $value)
            ->orderBy('c.Timestamp', 'DESC')
            ->setMaxResults(100)
            ->getQuery()
            ->getResult()
        ;
    }

    // /**
    //  * @return Flavour[] Returns an array of Flavour objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Flavour
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
