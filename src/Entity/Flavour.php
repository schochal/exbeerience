<?php

namespace App\Entity;

use App\Repository\FlavourRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: FlavourRepository::class)]
class Flavour
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: User::class)]
    private $Author;

    #[ORM\Column(type: 'float')]
    private $hoppy;

    #[ORM\Column(type: 'float')]
    private $malty;

    #[ORM\Column(type: 'float')]
    private $fruity;

    #[ORM\Column(type: 'float')]
    private $bitter;

    #[ORM\Column(type: 'float')]
    private $sweet;

    #[ORM\Column(type: 'float')]
    private $sour;

    #[ORM\ManyToOne(targetEntity: Beer::class, inversedBy: 'flavours')]
    #[ORM\JoinColumn(nullable: false)]
    private $Beer;

    #[ORM\Column(type: 'string', length: 255)]
    private $Timestamp;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAuthor(): ?User
    {
        return $this->Author;
    }

    public function setAuthor(?User $Author): self
    {
        $this->Author = $Author;

        return $this;
    }

    public function getHoppy(): ?float
    {
        return $this->hoppy;
    }

    public function setHoppy(float $hoppy): self
    {
        $this->hoppy = $hoppy;

        return $this;
    }

    public function getMalty(): ?float
    {
        return $this->malty;
    }

    public function setMalty(float $malty): self
    {
        $this->malty = $malty;

        return $this;
    }

    public function getFruity(): ?float
    {
        return $this->fruity;
    }

    public function setFruity(float $fruity): self
    {
        $this->fruity = $fruity;

        return $this;
    }

    public function getBitter(): ?float
    {
        return $this->bitter;
    }

    public function setBitter(float $bitter): self
    {
        $this->bitter = $bitter;

        return $this;
    }

    public function getSweet(): ?float
    {
        return $this->sweet;
    }

    public function setSweet(float $sweet): self
    {
        $this->sweet = $sweet;

        return $this;
    }

    public function getSour(): ?float
    {
        return $this->sour;
    }

    public function setSour(float $sour): self
    {
        $this->sour = $sour;

        return $this;
    }

    public function getBeer(): ?Beer
    {
        return $this->Beer;
    }

    public function setBeer(?Beer $Beer): self
    {
        $this->Beer = $Beer;

        return $this;
    }

    public function getTimestamp(): ?string
    {
        return $this->Timestamp;
    }

    public function setTimestamp(string $Timestamp): self
    {
        $this->Timestamp = $Timestamp;

        return $this;
    }
}
