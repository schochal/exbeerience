<?php

namespace App\Entity;

use App\Repository\BreweryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BreweryRepository::class)]
class Brewery
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $Name;

    #[ORM\Column(type: 'text', nullable: true)]
    private $Description;

    #[ORM\Column(type: 'string', length: 255)]
    private $Logo;

    #[ORM\Column(type: 'string', length: 255)]
    private $Homepage;

    #[ORM\OneToMany(mappedBy: 'Brewery', targetEntity: Beer::class)]
    private $beers;

    #[ORM\Column(type: 'boolean')]
    private $isTappedByBrewery;

    public function __construct()
    {
        $this->beers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(?string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->Logo;
    }

    public function setLogo(string $Logo): self
    {
        $this->Logo = $Logo;

        return $this;
    }

    public function getHomepage(): ?string
    {
        return $this->Homepage;
    }

    public function setHomepage(string $Homepage): self
    {
        $this->Homepage = $Homepage;

        return $this;
    }

    /**
     * @return Collection<int, Beer>
     */
    public function getBeers(): Collection
    {
        return $this->beers;
    }

    public function addBeer(Beer $beer): self
    {
        if (!$this->beers->contains($beer)) {
            $this->beers[] = $beer;
            $beer->setBrewery($this);
        }

        return $this;
    }

    public function removeBeer(Beer $beer): self
    {
        if ($this->beers->removeElement($beer)) {
            // set the owning side to null (unless already changed)
            if ($beer->getBrewery() === $this) {
                $beer->setBrewery(null);
            }
        }

        return $this;
    }

    public function getIsTappedByBrewery(): ?bool
    {
        return $this->isTappedByBrewery;
    }

    public function setIsTappedByBrewery(bool $isTappedByBrewery): self
    {
        $this->isTappedByBrewery = $isTappedByBrewery;

        return $this;
    }
}
