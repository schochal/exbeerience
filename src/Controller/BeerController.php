<?php

namespace App\Controller;

use App\Repository\BeerRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BeerController extends AbstractController
{
    #[Route('/beer/{slug}', name: 'beer')]
    public function index(string $slug, BeerRepository $br): Response
    {
				$beer = $br->findOneBySlug($slug);

        return $this->render('beer/index.html.twig', [
            'name' => $beer->getName(),
        ]);
    }
}
