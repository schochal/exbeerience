import React from 'react';
import * as ReactDOM from 'react-dom/client';

import {
	Alert,
	Box,
	Button,
  Container,
	Divider,
  Grid,
	List,
	ListItem,
	ListItemIcon,
	ListItemText,
  Typography,
} from '@mui/material';

import SportsBarIcon from '@mui/icons-material/SportsBar';
import PercentIcon from '@mui/icons-material/Percent';
import GradeIcon from '@mui/icons-material/Grade';
import StarIcon from '@mui/icons-material/Star';
import StarHalfIcon from '@mui/icons-material/StarHalf';
import StarOutlineIcon from '@mui/icons-material/StarOutline';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';

import ReactStars from "react-rating-stars-component";

import { ThemeProvider } from '@mui/material/styles';

import theme from './theme';

import axios from 'axios';

import { Radar } from 'react-chartjs-2';
import {
  Chart as ChartJS,
  RadialLinearScale,
  PointElement,
  LineElement,
  Filler,
  Tooltip,
  Legend,
} from 'chart.js';

ChartJS.register(
  RadialLinearScale,
  PointElement,
  LineElement,
  Filler,
  Tooltip,
  Legend
);

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
			beers: [],		
    };

		this.getData = this.getData.bind(this);
		this.calculateOverallFlavour = this.calculateOverallFlavour.bind(this);
  }

	componentDidMount() {
		this.getData();
	}

  async getData() {
    const response = await fetch('/api/profile');
    const data = await response.json();
    this.setState({
      beers: data,
    }, console.log(data));
  }

	calculateOverallFlavour(id) {
		if(this.state.beers[id].flavours.length == 0) {
			return;
		}
		let hoppy = 0;
		let malty = 0;
		let fruity = 0;
		let bitter = 0;
		let sweet = 0;
		let sour = 0;
		this.state.beers[id].flavours.map(flavour => {
			hoppy += flavour.hoppy;
			malty += flavour.malty;
			fruity += flavour.fruity;
			bitter += flavour.bitter;
			sweet += flavour.sweet;
			sour += flavour.sour;
		});
		const dataset = [
			hoppy / this.state.beers[id].flavours.length,
			malty / this.state.beers[id].flavours.length,
			fruity / this.state.beers[id].flavours.length,
			bitter / this.state.beers[id].flavours.length,
			sweet / this.state.beers[id].flavours.length,
			sour / this.state.beers[id].flavours.length,
		];

		return({
			labels: ['hoppy', 'malty', 'fruity', 'bitter', 'sweet', 'sour'],
			datasets: [
				{
					data: dataset,
					backgroundColor: 'rgba(210, 135, 18, 0.2)',
					borderColor: 'rgba(210, 135, 18, 1)',
					borderWidth: 1,
				},
			],
		});
	}


  render() {
		const options = {
			scale: {
				min: 0,
				max: 5,
			},
			plugins: {
				legend: {
					display: false
				}
			}
		};
	
    return (
      <ThemeProvider theme={theme}>
				<Container>
					<Button
						variant="contained"
						href='/'
						component="a"
						startIcon={<ArrowBackIcon/>}
						style={{ color: theme.palette.primary.contrastText, '&:hover': {color: theme.palette.primary.contrastText}, marginTop: '20px' }}
					>
						Beer List
					</Button>
					{this.state.beers.map((b, index) => (
						<Box key={b.beer.id} style={{ marginTop: '50px' }}>
							<Grid container spacing={2}>
								<Grid item xs={12} md={6}>
									<Typography variant="h3">
										{b.beer.name}
									</Typography>
									<Typography variant="h5" style={{ color: theme.palette.text.secondary }}>
										by {b.beer.brewery.name}
									</Typography>
									<List>
										<ListItem disablePadding>
											<ListItemIcon>
												<SportsBarIcon />
											</ListItemIcon>
											<ListItemText primary={b.beer.type} />
										</ListItem>
										<ListItem disablePadding>
											<ListItemIcon>
												<PercentIcon />
											</ListItemIcon>
											<ListItemText primary={b.beer.alc + "% vol"} />
										</ListItem>
									</List>
								</Grid>
								<Grid item xs={12} md={6}>
									<Box>
										{b.flavours.length != 0 ?
										<Radar 
											data={this.calculateOverallFlavour(index)}
											options={options}
										/> : <Alert severity="info">You can describe the flavour of this beer <a href={"/beer/" + b.beer.slug}>here</a></Alert> }
									</Box>
								</Grid>
							</Grid>
							{b.comments.length == 0 && <Alert severity="info" style={{ marginTop: '20px' }} >You can rate this beer <a href={"/beer/" + b.beer.slug}>here</a></Alert> }
							{b.comments.map((comment) => (
								<Box key={comment.id}>
									<Divider />
									<Typography sx={{ fontSize: 14, marginTop: '10px' }} color={theme.text.secondary} gutterBottom>
										{comment.Timestamp}
									</Typography>
									<ReactStars
										value={comment.Rating}
										count={5}
										size={20}
										isHalf={true}
										emptyIcon={<i className="far fa-star"></i>}
										halfIcon={<i className="fa fa-star-half-alt"></i>}
										fullIcon={<i className="fa fa-star"></i>}
										activeColor="#ffd700"
										edit={false}
									/>
									<Typography>
										{comment.Comment}	
									</Typography>
								</Box>
							))}
						</Box>
					))}
				</Container>
			</ThemeProvider>
    )
  }
}

const container = document.getElementById('profile');
const root = ReactDOM.createRoot(container);
root.render(<Profile/>);
