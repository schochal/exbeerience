import React from 'react';
import * as ReactDOM from 'react-dom/client';
import axios from 'axios';

import {
	Alert,
	Box,
	Button,
	Checkbox,
	Container,
	FormControlLabel,
	Grid,
	Snackbar,
	TextField,
	Typography,
} from '@mui/material';

import {ThemeProvider} from '@mui/material/styles';
import { styled } from '@mui/material/styles';
import theme from './theme';

import FileUploadIcon from '@mui/icons-material/FileUpload';

class EditBrewery extends React.Component {
  constructor(props) {
    super(props);

		this.state = {
			brewery: null,
			file: null,
			filename: '',
			snackbar: {
				message: '',
				severity: 'error',
				open: false,
			},
		}

		this.getData = this.getData.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.snackbar = this.snackbar.bind(this);
	}

	componentDidMount() {
		this.getData();
	}

  async getData() {
		let tmp = window.location.href.split('/');
		let f = '/api/edit/brewery/' + (tmp[tmp.length-1] == 'brewery' ? '0' : tmp[tmp.length-1]);
    const response = await fetch(f);
    const data = await response.json();
    this.setState({
      brewery: data,
    });
  }

	handleSubmit() {
		if(this.state.brewery.Name == '') {
			this.snackbar('please enter a name', 'error');
			return;
		}
		if(this.state.brewery.Description == '') {
			this.snackbar('please enter a description', 'error');
			return;
		}
		if(this.state.brewery.Homepage == '') {
			this.snackbar('please enter a homepage', 'error');
			return;
		}
		let tmp = window.location.href.split('/');
		if(this.state.file != null) {
			let formData = new FormData();
			formData.append('file', this.state.file);
			axios.post('/api/edit/update-brewery-image', formData, {
				headers: {
					"Content-Type": "multipart/form-data",
				},
			}).then((reponse) => {
				axios.put('/api/edit/update-brewery/' + tmp[tmp.length-1], this.state).then(response => {
					if(response.data.status == 'OK') {
						window.location = '/';
					} else {
						this.snackbar(response, 'error');
					}
				});
			});
		} else {
			axios.put('/api/edit/update-brewery/' + tmp[tmp.length-1], this.state).then(response => {
				if(response.data.status == 'OK') {
					window.location = '/';
				} else {
					this.snackbar(response, 'error');
				}
			});
		}
	}

	snackbar(message, severity) {
		this.setState({
			snackbar: {
				message: message,
				severity: severity,
				open: true,
			}
		});
	}

	render() {
		return (
      <ThemeProvider theme={theme}>
				{this.state.brewery != null &&
				<Container>
					<Typography variant="h3" style={{ marginTop: '20px' }}>
						Edit Brewery
					</Typography>
					<Grid container spacing={2} style={{ marginTop: '20px' }}>
						<Grid item xs={12} md={6}>
							<TextField
								variant="outlined"
								value={this.state.brewery.Name}
								onChange={(e) => {let f = {...this.state.brewery}; f.Name = e.target.value; this.setState({brewery: f})}}
								label="Name"
								fullWidth
							/>
						</Grid>
						<Grid item xs={12} md={6}>
							<TextField
								value={this.state.brewery.Homepage}
								onChange={(e) => {let f = {...this.state.brewery}; f.Homepage = e.target.value; this.setState({brewery: f})}}
								variant="outlined"
								label="Homepage"
								fullWidth
							/>
						</Grid>
						<Grid item xs={12} md={6}>
							<TextField
								value={this.state.brewery.Description}
								onChange={(e) => {let f = {...this.state.brewery}; f.Description = e.target.value; this.setState({brewery: f})}}
								variant="outlined"
								label="Description"
								fullWidth
								multiline
							/>
						</Grid>
						<Grid item xs={12} md={6}>
							<Button
								variant="contained"
								component="label"
								startIcon={<FileUploadIcon/>}
							>
								<input name="upload-photo" type="file" accept="image/*,*.svg" hidden onChange={(e) => this.setState({file: e.target.files[0], filename: e.target.files[0].name})}/>
								Upload Logo
							</Button>
							{this.state.file != null && 
								<Typography component="span">  Selected: {this.state.filename}</Typography>
							}
						</Grid>
						<Grid item xs={12}>
							<FormControlLabel
							  label="tapped by brewery"
								control={
									<Checkbox
										checked={this.state.brewery.isTappedByBrewery}
										onChange={(e) => {let f = {...this.state.brewery}; f.isTappedByBrewery = !f.isTappedByBrewery; this.setState({brewery: f})}}
									/>
								}
							/>
						</Grid>
						<Grid item xs={12} md={6}>
							<Button
								variant="contained"
								onClick={this.handleSubmit}
							>
								Submit
							</Button>
						</Grid>
					</Grid>
				</Container>}
				<Snackbar open={this.state.snackbar.open} autoHideDuration={6000} onClose={() => {this.setState({snackbar: {open: false}});}}>
					<Alert severity={this.state.snackbar.severity} sx={{ width: '100%' }}>
						{this.state.snackbar.message}
					</Alert>
				</Snackbar>
			</ThemeProvider>
		);
	}
}

const container = document.getElementById('edit');
const root = ReactDOM.createRoot(container);
root.render(<EditBrewery/>);
