import React from 'react';
import * as ReactDOM from 'react-dom/client';
import axios from 'axios';

import {
	Alert,
	Box,
	Button,
	Checkbox,
	Container,
	FormControl,
	FormControlLabel,
	Grid,
	InputAdornment,
	InputLabel,
	MenuItem,
	Select,
	Snackbar,
	TextField,
	Typography,
} from '@mui/material';

import {ThemeProvider} from '@mui/material/styles';
import { styled } from '@mui/material/styles';
import theme from './theme';

import FileUploadIcon from '@mui/icons-material/FileUpload';

class EditBeer extends React.Component {
  constructor(props) {
    super(props);

		this.state = {
			beer: null,
			file: null,
			filename: '',
			breweries: [],
			brewery: 0,
			snackbar: {
				message: '',
				severity: 'error',
				open: false,
			},
		}

		this.getData = this.getData.bind(this);
		this.getBreweries = this.getBreweries.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.snackbar = this.snackbar.bind(this);
	}

	componentDidMount() {
		this.getData();
		this.getBreweries();
	}

  async getData() {
		let tmp = window.location.href.split('/');
		let f = '/api/edit/beer/' + tmp[tmp.length-1];
    const response = await fetch(f);
    const data = await response.json();
    this.setState({
      beer: data,
    });
  }

  async getBreweries() {
		let f = '/api/brewery/';
    const response = await fetch(f);
    const data = await response.json();
		if(this.state.beer.Brewery != null) {
			this.setState({
				breweries: data,
				brewery: this.state.beer.Brewery.id,
			});
		} else {
			this.setState({
				breweries: data,
			});
		}
  }

	handleSubmit() {
		if(this.state.beer.Name == '') {
			this.snackbar('please enter a name', 'error');
			return;
		}
		if(this.state.beer.Description == '') {
			this.snackbar('please enter a description', 'error');
			return;
		}
		let tmp = window.location.href.split('/');
		axios.put('/api/edit/update-beer/' + tmp[tmp.length-1], this.state).then(response => {
			if(response.data.status == 'OK') {
				window.location = '/';
			} else {
				this.snackbar(response, 'error');
			}
		});
	}

	snackbar(message, severity) {
		this.setState({
			snackbar: {
				message: message,
				severity: severity,
				open: true,
			}
		});
	}

	render() {
		return (
      <ThemeProvider theme={theme}>
				{this.state.beer != null &&
				<Container>
					<Typography variant="h3" style={{ marginTop: '20px' }}>
						Edit Beer
					</Typography>
					<Grid container spacing={2} style={{ marginTop: '20px' }}>
						<Grid item xs={12} md={6}>
							<TextField
								variant="outlined"
								value={this.state.beer.Name}
								onChange={(e) => {let f = {...this.state.beer}; f.Name = e.target.value; this.setState({beer: f})}}
								label="Name"
								fullWidth
							/>
						</Grid>
						<Grid item xs={12} md={6}>
							<FormControl fullWidth>
								<InputLabel id="demo-simple-select-label">Brewery</InputLabel>
								<Select
									value={this.state.brewery}
									label="Brewery"
									onChange={(e) => {this.setState({brewery: e.target.value})}}
									fullWidth
								>
									<MenuItem value={0}><i>-- Select Brewery --</i></MenuItem>
									{this.state.breweries.map((brewery) => (
										<MenuItem value={brewery.id} key={brewery.id}>{brewery.Name}</MenuItem>
									))}
								</Select>
							</FormControl>
						</Grid>
						<Grid item xs={12} md={6}>
							<TextField
								variant="outlined"
								value={this.state.beer.Type}
								onChange={(e) => {let f = {...this.state.beer}; f.Type = e.target.value; this.setState({beer: f})}}
								label="Type"
								fullWidth
							/>
						</Grid>
						<Grid item xs={12} md={6}>
							<TextField
								variant="outlined"
								value={this.state.beer.Alc}
								onChange={(e) => {let f = {...this.state.beer}; f.Alc = e.target.value; this.setState({beer: f})}}
								label="Alcohol"
								InputProps={{
									endAdornment: <InputAdornment position="end">%vol</InputAdornment>,
								}}
								fullWidth
							/>
						</Grid>
						<Grid item xs={12} md={6}>
							<TextField
								variant="outlined"
								value={this.state.beer.Price}
								onChange={(e) => {let f = {...this.state.beer}; f.Price = e.target.value; this.setState({beer: f})}}
								label="Price"
								InputProps={{
									endAdornment: <InputAdornment position="end">CHF</InputAdornment>,
								}}
								type="number"
								fullWidth
							/>
						</Grid>
						<Grid item xs={12} md={6}>
							<TextField
								variant="outlined"
								value={this.state.beer.Slug}
								onChange={(e) => {let f = {...this.state.beer}; f.Slug = e.target.value; this.setState({beer: f})}}
								label='Slug (e.g. "maracuja_sour")'
								fullWidth
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								value={this.state.beer.Description}
								onChange={(e) => {let f = {...this.state.beer}; f.Description = e.target.value; this.setState({beer: f})}}
								variant="outlined"
								label="Description"
								fullWidth
								multiline
							/>
						</Grid>
						<Grid item xs={12}>
							<FormControlLabel
							  label="available right now"
								control={
									<Checkbox
										checked={this.state.beer.isTapped}
										onChange={(e) => {let f = {...this.state.beer}; f.isTapped = !f.isTapped; this.setState({beer: f})}}
									/>
								}
							/>
						</Grid>
						<Grid item xs={12} md={6}>
							<Button
								variant="contained"
								onClick={this.handleSubmit}
							>
								Submit
							</Button>
						</Grid>
					</Grid>
				</Container>}
				<Snackbar open={this.state.snackbar.open} autoHideDuration={6000} onClose={() => {this.setState({snackbar: {open: false}});}}>
					<Alert severity={this.state.snackbar.severity} sx={{ width: '100%' }}>
						{this.state.snackbar.message}
					</Alert>
				</Snackbar>
			</ThemeProvider>
		);
	}
}

const container = document.getElementById('edit_beer');
const root = ReactDOM.createRoot(container);
root.render(<EditBeer/>);
