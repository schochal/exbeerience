<?php

namespace App\Controller;

use App\Repository\BreweryRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class BreweryAPIController extends AbstractController
{
    #[Route('/api/brewery', name: 'brewery_api')]
    public function index(BreweryRepository $br): JsonResponse
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
 
        $breweries = $br->findAll();

        $json = $serializer->normalize($breweries, 'json', [
          'circular_reference_handler' => function ($object) {
            return $object->getId();
          }
        ]);

        return new JsonResponse($json);
    }
}
