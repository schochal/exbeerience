<?php

namespace App\Controller;

use App\Entity\Flavour;
use App\Repository\FlavourRepository;
use App\Repository\BeerRepository;

use Doctrine\Persistence\ManagerRegistry;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class FlavourAPIController extends AbstractController
{
    #[Route('/api/insert-flavour/{slug}', name: 'flavour_insert_api')]
    public function index(string $slug, Request $request, ManagerRegistry $doctrine, BeerRepository $br): Response
    {
        $data = $request->getContent();
        $data = json_decode($data);
  
        $user = $this->getUser();

        $beer = $br->findOneBySlug($slug);
        
        $date = new \DateTime();
        $timestamp = $date->format('Y-m-d_H-i-s');

        $flavour = new Flavour();
        $flavour->setHoppy($data->hoppy);
        $flavour->setMalty($data->malty);
        $flavour->setFruity($data->fruity);
        $flavour->setBitter($data->bitter);
        $flavour->setSweet($data->sweet);
        $flavour->setSour($data->sour);
        $flavour->setBeer($beer);  
        $flavour->setTimestamp($timestamp);  
        if(null !== $user) {
            $flavour->setAuthor($user);
        } 

        $em = $doctrine->getManager();
        $em->persist($flavour);
        $em->flush();

        return $this->json([
          'message' => 'your flavour profile has been added!',
          'status' => 'OK',
        ]);
    }

    #[Route('/api/flavour/{slug}', name: 'flavour_api')]
    public function get_flavour(string $slug, FlavourRepository $fr, BeerRepository $br): JsonResponse
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
 
        $beer = $br->findOneBySlug($slug);
        $flavours = $fr->findByBeer($beer);
  
        foreach($flavours as $flavour) {
          $flavour->setBeer(null);
        }

        $json = $serializer->normalize($flavours, 'json', [
          'circular_reference_handler' => function ($object) {
            return $object->getId();
          }
        ]);

        return new JsonResponse($json);
    }
}
