<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Repository\BeerRepository;
use App\Repository\CommentRepository;

use Doctrine\Persistence\ManagerRegistry;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class CommentAPIController extends AbstractController
{
    #[Route('/api/insert-comment', name: 'comment_insert_api')]
    public function index(Request $request, ManagerRegistry $doctrine, BeerRepository $br): Response
    {
        $data = $request->getContent();
        $data = json_decode($data);
  
        $user = $this->getUser();

        $beer = $br->findOneBySlug($data->beer->Slug);
        
        $date = new \DateTime();
        $timestamp = $date->format('D, F jS Y, H:i');
        $timestampSort = $date->format('Y-m-d_H-i-s');

        $comment = new Comment();
        $comment->setComment($data->comment);  
        $comment->setRating($data->rating);  
        $comment->setName($data->name);  
        $comment->setBeer($beer);  
        $comment->setTimestamp($timestamp);  
        $comment->setTimestampSort($timestampSort);
        $comment->setIsPublic(true);
        if(null !== $user) {
            $comment->setAuthor($user);
            $comment->setIsPublic($data->public);
        } 

        $em = $doctrine->getManager();
        $em->persist($comment);
        $em->flush();

        return $this->json([
          'message' => 'your comment has been added!',
          'status' => 'OK',
        ]);
    }

    #[Route('/api/comment/{slug}', name: 'comment_api')]
    public function get_comments(string $slug, CommentRepository $cr, BeerRepository $br): JsonResponse
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
 
        $beer = $br->findOneBySlug($slug);
        $comments = $cr->findByBeer($beer);
  
        foreach($comments as $comment) {
          $comment->setBeer(null);
        }

        $json = $serializer->normalize($comments, 'json', [
          'circular_reference_handler' => function ($object) {
            return $object->getId();
          }
        ]);

        return new JsonResponse($json);
    }

    #[Route('/api/edit/delete_comment/{id}', name: 'delete_comment_api')]
    public function delete_comment(int $id, CommentRepository $cr, ManagerRegistry $doctrine): Response
    {
        $comment = $cr->findOneById($id);
        $em = $doctrine->getManager();
        $em->remove($comment);
        $em->flush();
        return new Response('OK');
    }
}
