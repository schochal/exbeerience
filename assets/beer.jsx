import React from 'react';
import * as ReactDOM from 'react-dom/client';
import axios from 'axios';
import ReactStars from "react-rating-stars-component";

import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
	Alert,
	Box,
	Button,
	Checkbox,
	Container,
	Divider,
	FormControlLabel,
	Grid,
	List,
	ListItem,
	ListItemIcon,
	ListItemText,
	Slider,
	Snackbar,
	TextField,
	Typography,
} from '@mui/material';

import {ThemeProvider} from '@mui/material/styles';
import { styled } from '@mui/material/styles';
import theme from './theme';

import SportsBarIcon from '@mui/icons-material/SportsBar';
import PercentIcon from '@mui/icons-material/Percent';
import GradeIcon from '@mui/icons-material/Grade';
import StarIcon from '@mui/icons-material/Star';
import StarHalfIcon from '@mui/icons-material/StarHalf';
import StarOutlineIcon from '@mui/icons-material/StarOutline';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import AttachMoneyIcon from '@mui/icons-material/AttachMoney';
import ClearIcon from '@mui/icons-material/Clear';

import { Radar } from 'react-chartjs-2';
import {
  Chart as ChartJS,
  RadialLinearScale,
  PointElement,
  LineElement,
  Filler,
  Tooltip,
  Legend,
} from 'chart.js';

ChartJS.register(
  RadialLinearScale,
  PointElement,
  LineElement,
  Filler,
  Tooltip,
  Legend
);

class Beer extends React.Component {
  constructor(props) {
    super(props);

		this.state = {
			beer: null,
			rating: 0,
			overallRating: 0,
			ratings: null,
			name: '',
			comment: '',
			comments: [],
			snackbar: {
				message: '',
				severity: 'error',
				open: false,
			},
			user: {},
			public: false,
			flavour: {
				hoppy: 3,
				malty: 3,
				fruity: 3,
				bitter: 3,
				sweet: 3,
				sour: 3,
			},
			flavours: [],
			overallFlavour: null,
		}

		this.getData = this.getData.bind(this);
		this.getComments = this.getComments.bind(this);
		this.calculateOverallRating = this.calculateOverallRating.bind(this);
		this.calculateOverallFlavour = this.calculateOverallFlavour.bind(this);
		this.getRating = this.getRating.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleSubmitFlavour = this.handleSubmitFlavour.bind(this);
		this.snackbar = this.snackbar.bind(this);
		this.deleteComment = this.deleteComment.bind(this);

		this.getUser = this.getUser.bind(this);
		this.isLoggedIn = this.isLoggedIn.bind(this);
		this.isAdmin = this.isAdmin.bind(this);
	}

	componentDidMount() {
		this.getData();
		this.getComments();
		this.getFlavour();
		this.getUser();
	}

  async getData() {
		let tmp = window.location.href.split('/');
    const response = await fetch('/api/beer/' + tmp[tmp.length-1]);
    const data = await response.json();
    this.setState({
      beer: data,
    });
  }

  async getComments() {
		let tmp = window.location.href.split('/');
    const response = await fetch('/api/comment/' + tmp[tmp.length-1]);
    const data = await response.json();
    this.setState({
      comments: data,
    }, this.calculateOverallRating);
  }

  async getFlavour() {
		let tmp = window.location.href.split('/');
    const response = await fetch('/api/flavour/' + tmp[tmp.length-1]);
    const data = await response.json();
    this.setState({
      flavours: data,
    }, this.calculateOverallFlavour);
  }

  async handleSubmit() {
		if(this.state.rating == 0) {
			this.snackbar('Please enter a rating', 'error');
			return;
		}
    if(this.state.name == '' && !this.isLoggedIn()) {
			this.snackbar('Please enter a name', 'error');
			return;
		}
		if(this.state.comment == '') {
			this.snackbar('Please enter a comment', 'error');
			return;
		}
		axios.put('/api/insert-comment', this.state).then((response) => {
			if(response.data.status == 'OK') {
				this.snackbar(response.data.message, 'success');
				this.getComments();
				this.setState({
					name: '',
					comment: '',
					rating: 0,
					public: false,
				});
			} else {
				this.snackbar(response.data.message, 'error');
			}
		});
	}

  async handleSubmitFlavour() {
		axios.put('/api/insert-flavour/' + this.state.beer.Slug, this.state.flavour).then((response) => {
			if(response.data.status == 'OK') {
				this.snackbar(response.data.message, 'success');
				this.getFlavour();
				this.setState({
					flavour: {
						hoppy: 3,
						malty: 3,
						fruity: 3,
						bitter: 3,
						sweet: 3,
						sour: 3,
					}
				});
			} else {
				this.snackbar(response.data.message, 'error');
			}
		});
	}

	snackbar(message, severity) {
		this.setState({
			snackbar: {
				message: message,
				severity: severity,
				open: true,
			}
		});
	}

	calculateOverallRating() {
		let overall = 0;
		this.state.comments.map(comment => {
			overall += comment.Rating;
		});
		if(this.state.comments.length != 0) {
			this.setState({
				overallRating: overall / this.state.comments.length,
			});
		} else {
			this.setState({
				overallRating: 0,
			});
		}
	}

	getRating() {
		let round = Math.round(this.state.overallRating * 2) / 2;
		let stars = [];
		let iterator = 0;
		for(let i = 0; i < Math.floor(round); i++) {
			stars.push(<StarIcon key={iterator}/>);
			iterator++;
		}
		if(round - Math.floor(round) > 0.3) {	
			stars.push(<StarHalfIcon key={iterator}/>);
			iterator++;
		}
		for(let i = stars.length; i < 5; i++) {
			stars.push(<StarOutlineIcon key={iterator}/>);
			iterator++;
		}
		return stars;
	}

	calculateOverallFlavour() {
		if(this.state.flavours.length == 0) {
			return;
		}
		let hoppy = 0;
		let malty = 0;
		let fruity = 0;
		let bitter = 0;
		let sweet = 0;
		let sour = 0;
		this.state.flavours.map(flavour => {
			hoppy += flavour.hoppy;
			malty += flavour.malty;
			fruity += flavour.fruity;
			bitter += flavour.bitter;
			sweet += flavour.sweet;
			sour += flavour.sour;
		});
		const dataset = [
			hoppy / this.state.flavours.length,
			malty / this.state.flavours.length,
			fruity / this.state.flavours.length,
			bitter / this.state.flavours.length,
			sweet / this.state.flavours.length,
			sour / this.state.flavours.length,
		];

		this.setState({
			overallFlavour: {
				labels: ['hoppy', 'malty', 'fruity', 'bitter', 'sweet', 'sour'],
				datasets: [
					{
						data: dataset,
						backgroundColor: 'rgba(210, 135, 18, 0.2)',
						borderColor: 'rgba(210, 135, 18, 1)',
						borderWidth: 1,
					},
				],
			}
		});
	}

	isLoggedIn() {
		if(Object.entries(this.state.user).length == 0)
			return false;
		return true;
	}

	async getUser() {
    const response = await fetch('/api/user');
    const data = await response.json();
    this.setState({
      user: data,
		});
	}

	isAdmin() {
		if(this.isLoggedIn()) {
			if(this.state.user.roles.includes('ROLE_ADMIN')) {
				return true;
			}
		}
		return false;
	}

	deleteComment(e) {
		axios.post('/api/edit/delete_comment/' + e).then(this.getComments());
	}

	render() {
		const marks = [
			{value: 1, label: '1'},
			{value: 2, label: '2'},
			{value: 3, label: '3'},
			{value: 4, label: '4'},
			{value: 5, label: '5'},
		];
		const options = {
			scale: {
				min: 0,
				max: 5,
			},
			plugins: {
				legend: {
					display: false
				}
			}
		};
		return (
      <ThemeProvider theme={theme}>
				{this.state.beer != null &&
				<Container>
					<Grid container spacing={2} style={{ marginTop: '10px' }}>
						<Grid item xs={12} md={6}>
							<Typography variant="h3" style={{ marginTop: '50px' }}>
								{this.state.beer.Name}
							</Typography>
							<Typography variant="h5" style={{ color: theme.palette.text.secondary }}>
								by {this.state.beer.Brewery.name}
							</Typography>
							<List>
								<ListItem disablePadding>
									<ListItemIcon>
										<SportsBarIcon />
									</ListItemIcon>
									<ListItemText primary={this.state.beer.Type} />
								</ListItem>
								<ListItem disablePadding>
									<ListItemIcon>
										<PercentIcon />
									</ListItemIcon>
									<ListItemText primary={this.state.beer.Alc + "% vol"} />
								</ListItem>
								<ListItem disablePadding>
									<ListItemIcon>
										<AttachMoneyIcon />
									</ListItemIcon>
									<ListItemText primary={this.state.beer.Price + " CHF"} />
								</ListItem>
								<ListItem disablePadding>
									<ListItemIcon>
										<GradeIcon />
									</ListItemIcon>
									<ListItemText primary={this.getRating()} />
								</ListItem>
							</List>
					
							<Typography>
								{this.state.beer.Description}
							</Typography>
						</Grid>
						<Grid item xs={12} md={6}>
							<Box>
								{this.state.overallFlavour != null &&
								<Radar 
									data={this.state.overallFlavour}
									options={options}
								/>}
							</Box>
						</Grid>
					</Grid>

					<Accordion style={{ marginTop: '20px' }} elevation={3}>
						<AccordionSummary
							expandIcon={<ExpandMoreIcon/>}
							aria-controls="panel1a-content"
						>
							<Typography variant="h5">
								This beer is...
							</Typography>
						</AccordionSummary>
						<AccordionDetails>
							<Grid container spacing={2}>
								{!this.isLoggedIn() && <Grid item xs={12}>
									<Alert severity="info">
										If you <a href="/login">Log in</a>, you'll be able to see all your ratings and tastings under "My Beers"
									</Alert>
								</Grid>}
								<Grid item xs={12} md={6}>
									<Typography variant="h6">
										...hoppy
									</Typography>
									<Slider 
										min={1}
										max={5}
										step={.5}
										marks={marks}
										value={this.state.flavour.hoppy}
										onChange={(ev, e) => {let f = {...this.state.flavour}; f.hoppy = e; this.setState({flavour: f})}}
									/>
								</Grid>
								<Grid item xs={12} md={6}>
									<Typography variant="h6">
										...malty
									</Typography>
									<Slider 
										min={1}
										max={5}
										step={.5}
										marks={marks}
										value={this.state.flavour.malty}
										onChange={(ev, e) => {let f = {...this.state.flavour}; f.malty = e; this.setState({flavour: f})}}
									/>
								</Grid>
								<Grid item xs={12} md={6}>
									<Typography variant="h6">
										...fruity
									</Typography>
									<Slider 
										min={1}
										max={5}
										step={.5}
										marks={marks}
										value={this.state.flavour.fruity}
										onChange={(ev, e) => {let f = {...this.state.flavour}; f.fruity = e; this.setState({flavour: f})}}
									/>
								</Grid>
								<Grid item xs={12} md={6}>
									<Typography variant="h6">
										...bitter
									</Typography>
									<Slider 
										min={1}
										max={5}
										step={.5}
										marks={marks}
										value={this.state.flavour.bitter}
										onChange={(ev, e) => {let f = {...this.state.flavour}; f.bitter = e; this.setState({flavour: f})}}
									/>
								</Grid>
								<Grid item xs={12} md={6}>
									<Typography variant="h6">
										...sweet
									</Typography>
									<Slider 
										min={1}
										max={5}
										step={.5}
										marks={marks}
										value={this.state.flavour.sweet}
										onChange={(ev, e) => {let f = {...this.state.flavour}; f.sweet = e; this.setState({flavour: f})}}
									/>
								</Grid>
								<Grid item xs={12} md={6}>
									<Typography variant="h6">
										...sour
									</Typography>
									<Slider 
										min={1}
										max={5}
										step={.5}
										marks={marks}
										value={this.state.flavour.sour}
										onChange={(ev, e) => {let f = {...this.state.flavour}; f.sour = e; this.setState({flavour: f})}}
									/>
								</Grid>
								<Grid item xs={12}>
									<Button 
										variant="contained"
										onClick={this.handleSubmitFlavour}
									>
										Submit
									</Button>
								</Grid>
							</Grid>
						</AccordionDetails>
					</Accordion>
					<Accordion>
						<AccordionSummary
							expandIcon={<ExpandMoreIcon/>}
							aria-controls="panel1a-content"
						>
							<Typography variant="h5">
								Rate this Beer
							</Typography>
						</AccordionSummary>
						<AccordionDetails>
							<Grid container spacing={2}>
								{!this.isLoggedIn() && <Grid item xs={12}>
									<Alert severity="info">
										If you <a href="/login">Log in</a>, you'll be able to see all your ratings and tastings under "My Beers"
									</Alert>
								</Grid>}
								<Grid item xs={12}>
									<ReactStars
										count={5}
										value={this.state.rating}
										onChange={(e) => {this.setState({rating: e})}}
										size={50}
										isHalf={true}
										emptyIcon={<i className="far fa-star"></i>}
										halfIcon={<i className="fa fa-star-half-alt"></i>}
										fullIcon={<i className="fa fa-star"></i>}
										activeColor="#ffd700"
									/>
								</Grid>
								{!this.isLoggedIn() &&
								<Grid item xs={12}>
									<TextField
										variant="outlined"
										value={this.state.name}
										onChange={(e) => {this.setState({name: e.target.value})}}
										label="Name"
										fullWidth
									/>
								</Grid>}
								<Grid item xs={12}>
									<TextField
										variant="outlined"
										value={this.state.comment}
										onChange={(e) => {this.setState({comment: e.target.value})}}
										label="Comment"
										fullWidth
										multiline
										rows={3}
									/>
								</Grid>
								{this.isLoggedIn() &&
								<Grid item>
									<FormControlLabel control={<Checkbox checked={this.state.public} onChange={() => this.setState({public: !this.state.public})} />} label="Publicly Visible" />
								</Grid>}
								<Grid item xs={12}>
									<Button
										variant="contained"
										onClick={this.handleSubmit}
									>
										Submit
									</Button>
								</Grid>
							</Grid>
						</AccordionDetails>
					</Accordion>
					<br/>
					{this.state.comments.map(comment => (
						<Box style={{ marginTop: '10px' }} key={comment.id}>
							<Divider />
							<Typography sx={{ fontSize: 14, marginTop: '10px' }} color={theme.text.secondary} gutterBottom>
								{(comment.Author != null) ? comment.Author.firstName + ' ' + comment.Author.lastName + ' (' + comment.Author.username + ')' : comment.Name} at {comment.Timestamp}
							</Typography>
							<ReactStars
								value={comment.Rating}
								count={5}
								size={20}
								isHalf={true}
								emptyIcon={<i className="far fa-star"></i>}
								halfIcon={<i className="fa fa-star-half-alt"></i>}
								fullIcon={<i className="fa fa-star"></i>}
								activeColor="#ffd700"
								edit={false}
							/>
							<Typography>
								{comment.Comment}	
							</Typography>
							{this.isAdmin() &&
							<Button
								onClick={() => {this.deleteComment(comment.id)}}
								value={comment.id}
								variant="outlined"
								color="error"
							>
								<ClearIcon/>
							</Button>}
						</Box>
					))}
				</Container>}
				<Snackbar open={this.state.snackbar.open} autoHideDuration={6000} onClose={() => {this.setState({snackbar: {open: false}});}}>
					<Alert severity={this.state.snackbar.severity} sx={{ width: '100%' }}>
						{this.state.snackbar.message}
					</Alert>
				</Snackbar>
			</ThemeProvider>
		);
	}
}

const container = document.getElementById('beer');
const root = ReactDOM.createRoot(container);
root.render(<Beer/>);
