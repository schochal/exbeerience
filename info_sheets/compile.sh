#!/bin/bash

DATA=data/beerInfo.csv
OUTPUT=output/
QR=qrcodes/
TEX=tex/beerInfo.tex

mkdir -p tmp
cp tex/icons tmp -r

while read line
do
	name=$( echo $line | cut -d ',' -f 1 )
	brewery=$( echo $line | cut -d ',' -f 2 )
	type=$( echo $line | cut -d ',' -f 3 )
	alc=$( echo $line | cut -d ',' -f 4 )
	price=$( echo $line | cut -d ',' -f 5 )
	slug=$( echo $line | cut -d ',' -f 6 )

	echo "Processing $name"

	url="https://exbeerience.aschoch.ch/beer/$slug"
	url_esc=$( echo "$url" | sed 's/\//\\\//g' | sed 's/_/\\\\_/g' )

	cd "$QR"
	qrencode "$url" -m 0 -t SVG -o "$slug.svg"
	inkscape "$slug.svg" --export-type=pdf -o "$slug.pdf"
	rm "$slug.svg"
	cd '..'

	cp $TEX "tmp/$slug.tex"
	cd tmp
	jeton=CHF
	#[[ $price -eq 1 ]] && jeton=Jeton
	sed -i "s/NAME/$name/g" "$slug.tex"
	sed -i "s/BREWERY/$brewery/g" "$slug.tex"

	sed -i "s/TYPE/$type/g" "$slug.tex"
	sed -i "s/ALC/$alc/g" "$slug.tex"
	sed -i "s/PRICE/$price $jeton/g" "$slug.tex"

	sed -i "s/SLUG/$slug/g" "$slug.tex"
	sed -i "s/URL/$url_esc/g" "$slug.tex"
	lualatex "$slug.tex" > /dev/null
	mv $slug.pdf ../output/
	cd '..'
	
done < "$DATA"

rm -r tmp
