# ExBeerience

A WebApp for the ExBeerience 2022. 

Features:
- ✔ A Main page showing the attending Breweries
	- ✔ Links to each beer 
- ✔ A Page for each Beer with descriptions
	- ✔ Allow people to comment on and rate the beers
	- ☐ Allow people to show the flavour profile
	- ☐ Generate a "spider web" from the flavour profile
- ☐ Add and Edit Breweries
- ☐ Add and Edit Beers
- ☐ Login for the Board Members


## How to Deploy

The Application is written with the following technologies:

- Backend: php/Symfony 
- Frontend: React
	- UI Library: mui

First, you have to clone the repository:

```
git clone https://gitlab.ethz.ch/schochal/exbeerience
cd exbeerience
```

In order to configure the environment, we need to define two symfony variables in a new file `.env.local`:
```
APP_ENV=prod
APP_SECRET=34354cd17524811a8f97f21d63884829

DATABASE_URL="mysql://username:password@127.0.0.1:port/exbeerience?serverVersion=mariadb-10.5.13&charset=utf8"
```

`APP_ENV` can be changed to `dev` for debugging.

Make sure to change the database (here: mysql/mariadb), username, password and port to the according values.


Next, php packages have to be installed. For that, run:

```
composer install
```

To set up the database, run

```
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
```

Now, the JavaScript dependencies have to be installed via:

```
yarn install
```

Build the JavaScript code via (this is typically not done on production)

```
yarn encore dev
```

The root of the application is found under `public/index.php` (important for nginx config). Template symfony nginx config: https://www.nginx.com/resources/wiki/start/topics/recipes/symfony/


