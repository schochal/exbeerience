<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220430182812 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE flavour (id DOUBLE PRECISION NOT NULL, author_id INT DEFAULT NULL, hoppy DOUBLE PRECISION NOT NULL, malty DOUBLE PRECISION NOT NULL, fruity DOUBLE PRECISION NOT NULL, bitter DOUBLE PRECISION NOT NULL, sweet DOUBLE PRECISION NOT NULL, sour DOUBLE PRECISION NOT NULL, INDEX IDX_4FF2D983F675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE flavour ADD CONSTRAINT FK_4FF2D983F675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE flavour');
    }
}
