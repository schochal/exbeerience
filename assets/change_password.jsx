import React from 'react';
import * as ReactDOM from 'react-dom/client';

import {
  Alert,
  Button,
  Container,
  Grid,
  Snackbar,
  TextField,
  Typography,
} from '@mui/material';

import { ThemeProvider } from '@mui/material/styles';

import theme from './theme';

import axios from 'axios';

class ChangePassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
			password1: '',
			password2: '',
      snackbar: false,
      snackbarError: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
		e.preventDefault();
    if(this.state.password1 != '' && this.state.password1 != '') {
			if(this.state.password1 == this.state.password2) {
				axios.post('/api/change-password', {
					password: this.state.password1,
				}).then(response => {
					console.log(response);
					this.setState({snackbar: true});
				});
			}
    } else {
      this.setState({snackbarError: true});
    }
  }

  render() {
    return (
      <ThemeProvider theme={theme}>
				<Container>
					<Typography variant="h4" style={{ marginTop: '20px' }}>Change Password</Typography>
					<form>
						<Grid container spacing={2} style={{ marginTop: '20px' }}>
							{this.state.password1 != this.state.password2 && 
							<Grid item xs={12}>
								<Alert severity="error">The passwords do not match</Alert>
							</Grid> }
							<Grid item xs={12} md={6}>
								<TextField
									label="New Password"
									variant="outlined"
									type="password"
									value={this.state.password1}
									onChange={(e) => this.setState({password1: e.target.value})}
									fullWidth
								/>
							</Grid>
							<Grid item xs={12} md={6}>
								<TextField
									label="Retype Password"
									variant="outlined"
									value={this.state.password2}
									type="password"
									onChange={(e) => this.setState({password2: e.target.value})}
									fullWidth
								/>
							</Grid>
							<Grid item xs={12}>
								<Button
									type="submit"
									variant="contained"
									onClick={(e) => {this.handleSubmit(e)}}
								>
									Submit
								</Button>
							</Grid>
						</Grid>
					</form>
				</Container>
        <Snackbar open={this.state.snackbar} autoHideDuration={6000} onClose={() => {this.setState({snackbar: false});}}>
          <Alert severity="success" sx={{ width: '100%' }}>
           The Password was successfully changed! 
          </Alert>
        </Snackbar>
        <Snackbar open={this.state.snackbarError} autoHideDuration={6000} onClose={() => {this.setState({snackbarError: false});}}>
          <Alert severity="error" sx={{ width: '100%' }}>
           Please completely fill out the form
          </Alert>
        </Snackbar>
			</ThemeProvider>
    )
  }
}

const container = document.getElementById('change_password');
const root = ReactDOM.createRoot(container);
root.render(<ChangePassword/>);
