import React from 'react';
import * as ReactDOM from 'react-dom/client';
import axios from 'axios';

import {
	Accordion,
	AccordionSummary,
	AccordionDetails,
	Alert,
	Box,
	Button,
	Card,
	CardActions,
	CardContent,
	Container,
	Grid,
	List,
	ListItem,
	ListItemIcon,
	ListItemText,
	Typography,
} from '@mui/material';

import Masonry from '@mui/lab/Masonry';

import {ThemeProvider} from '@mui/material/styles';
import { styled } from '@mui/material/styles';
import theme from './theme';

import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import InfoIcon from '@mui/icons-material/Info';
import LaunchIcon from '@mui/icons-material/Launch';
import SportsBarIcon from '@mui/icons-material/SportsBar';
import PercentIcon from '@mui/icons-material/Percent';
import EditIcon from '@mui/icons-material/Edit';
import AddIcon from '@mui/icons-material/Add';
import AttachMoneyIcon from '@mui/icons-material/AttachMoney';

class Main extends React.Component {
  constructor(props) {
    super(props);

		this.state = {
			breweries: [],
			beers: [],
			untapped: [],
			user: {},
		}

		this.getBreweries = this.getBreweries.bind(this);
		this.getBeers = this.getBeers.bind(this);
		this.getUntapped = this.getUntapped.bind(this);

		this.getUser = this.getUser.bind(this);
		this.isLoggedIn = this.isLoggedIn.bind(this);
		this.isAdmin = this.isAdmin.bind(this);

		this.getBeerInfo = this.getBeerInfo.bind(this);
		this.getUntappedInfo = this.getUntappedInfo.bind(this);
	}

	componentDidMount() {
		this.getBreweries();
		this.getBeers();
		this.getUntapped();
		this.getUser();
	}

  async getBreweries() {
    const response = await fetch('/api/brewery');
    const data = await response.json();
    this.setState({
      breweries: data,
    });
  }

  async getBeers() {
    const response = await fetch('/api/beers');
    const data = await response.json();
    this.setState({
      beers: data,
    });
  }

  async getUntapped() {
    const response = await fetch('/api/beers_untapped');
    const data = await response.json();
    this.setState({
      untapped: data,
    });
  }

	isLoggedIn() {
		if(Object.entries(this.state.user).length == 0)
			return false;
		return true;
	}

	isAdmin() {
		if(this.isLoggedIn()) {
			if(this.state.user.roles.includes('ROLE_ADMIN')) {
				return true;
			}
		}
		return false;
	}

	async getUser() {
    const response = await fetch('/api/user');
    const data = await response.json();
    this.setState({
      user: data,
		});
	}
	
	getBeerInfo() { // export the relevant beer info to csv
		let text = '';
		this.state.beers.map((beer) => {
			text += beer.Name + ',' + beer.Brewery.name + ',' + beer.Type + ',' + beer.Alc + ',' + beer.Price + ',' + beer.Slug + '\n';
		});
		let element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', 'beerInfo.csv');

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
	}

	getUntappedInfo() { // export the relevant beer info to csv
		let text = '';
		this.state.untapped.map((beer) => {
			text += beer.Name + ',' + beer.Brewery.name + ',' + beer.Type + ',' + beer.Alc + ',' + beer.Price + ',' + beer.Slug + '\n';
		});
		let element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', 'beerInfo.csv');

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
	}

	render() {
		return (
      <ThemeProvider theme={theme}>
				<Box style={{ backgroundColor: theme.palette.primary.dark, padding: '20px' }}>
					<Container>
						<Grid container spacing={2} style={{ justify: 'flex-end', alignItems: 'center' }}>
							<Grid item xs={12} md={6} >
								<Typography variant="h2" style={{ color: theme.palette.primary.contrastText, textAlign: (window.innerWidth > 900) ? 'right' : 'left' }}>
									Welcome to the <br/>
									<span style={{ color: theme.palette.primary.main }}> Herbst<b>Beer</b>ience 2022</span>!
								</Typography>
							</Grid>
							<Grid item xs={12} md={6}>
								<img src="glass.svg" style={{ width: '100%' }}/>
							</Grid>
						</Grid>
					</Container>
				</Box>

				<Container>
					<Button
						onClick={() => {window.location.hash = 'breweries'}}
						variant="contained"
						style={{ marginTop: '20px' }}
					>
						Go to Breweries
					</Button>
					<div id="beers"></div>
					<Typography variant="h3" style={{ marginTop: '50px', marginBottom: '20px', color: theme.palette.primary.dark }}>The Beers {this.isAdmin() && <><Button variant="outlined" onClick={() => {window.location = '/edit/beer/0'}}><AddIcon/></Button><Button variant="outlined" onClick={this.getBeerInfo} style={{ marginLeft: '10px' }}>Get Beer Data</Button></>}</Typography>
          <Box style={{ marginTop: '20px', marginRight: '-16px' }}>
					<Masonry columns={{xs: 1, md: 2}} spacing={2}>
						{this.state.beers.map((beer) => (
							<Card key={beer.id} elevation={3}>
								<CardContent>
									<Typography variant="h4">
										{beer.Name} {this.isAdmin() && <Button variant="outlined" onClick={() => {window.location = '/edit/beer/' + beer.id}}><EditIcon/></Button>}
									</Typography>
									<Typography variant="h5" style={{ color: theme.palette.text.secondary }}>
										by {beer.Brewery.name}
									</Typography>
									{!beer.Brewery.isTappedByBrewery && <Alert severity="info" style={{ marginTop: '10px', marginBottom: '10px' }}>This beer is tapped by the ExBeerience staff, not the brewery itself</Alert>}
									<List>
										<ListItem disablePadding>
											<ListItemIcon>
												<SportsBarIcon />
											</ListItemIcon>
											<ListItemText primary={beer.Type} />
										</ListItem>
										<ListItem disablePadding>
											<ListItemIcon>
												<PercentIcon />
											</ListItemIcon>
											<ListItemText primary={beer.Alc + "% vol"} />
										</ListItem>
										<ListItem disablePadding>
											<ListItemIcon>
												<AttachMoneyIcon />
											</ListItemIcon>
											<ListItemText primary={beer.Price + " CHF"} />
										</ListItem>
									</List>
									<Typography
										style={{ whiteSpace: 'pre-wrap' }}
									>
										{beer.Description}
									</Typography>
								</CardContent>
								<CardActions>
									<Box style={{ marginTop: '10px' }}>
										<Button component="a" href={"/beer/" + beer.Slug} endIcon={<LaunchIcon/>} variant="contained" style={{ color: theme.palette.primary.contrastText, '&:hover': {color: theme.palette.primary.contrastText} }}>View this Beer</Button>
									</Box>
								</CardActions>
							</Card>
						))}
					</Masonry>
					</Box>

					{/* Untapped Beers */}
					{this.isAdmin() &&
					<Box>
					<Typography variant="h3" style={{ marginTop: '50px', marginBottom: '20px', color: theme.palette.primary.dark }}>Untapped Beers {this.isAdmin() && <><Button variant="outlined" onClick={() => {window.location = '/edit/beer/0'}}><AddIcon/></Button><Button variant="outlined" onClick={this.getUntappedInfo} style={{ marginLeft: '10px' }}>Get Beer Data</Button></>}</Typography>
          <Box style={{ marginTop: '20px', marginRight: '-16px' }}>
					<Masonry columns={{xs: 1, md: 2}} spacing={2}>
						{this.state.untapped.map((beer) => (
							<Card key={beer.id} elevation={3}>
								<CardContent>
									<Typography variant="h4">
										{beer.Name} {this.isAdmin() && <Button variant="outlined" onClick={() => {window.location = '/edit/beer/' + beer.id}}><EditIcon/></Button>}
									</Typography>
									<Typography variant="h5" style={{ color: theme.palette.text.secondary }}>
										by {beer.Brewery.name}
									</Typography>
									{!beer.Brewery.isTappedByBrewery && <Alert severity="info" style={{ marginTop: '10px', marginBottom: '10px' }}>This beer is tapped by the ExBeerience staff, not the brewery itself</Alert>}
									<List>
										<ListItem disablePadding>
											<ListItemIcon>
												<SportsBarIcon />
											</ListItemIcon>
											<ListItemText primary={beer.Type} />
										</ListItem>
										<ListItem disablePadding>
											<ListItemIcon>
												<PercentIcon />
											</ListItemIcon>
											<ListItemText primary={beer.Alc + "% vol"} />
										</ListItem>
										<ListItem disablePadding>
											<ListItemIcon>
												<AttachMoneyIcon />
											</ListItemIcon>
											<ListItemText primary={beer.Price + " CHF"} />
										</ListItem>
									</List>
									<Typography
										style={{ whiteSpace: 'pre-wrap' }}
									>
										{beer.Description}
									</Typography>
								</CardContent>
								<CardActions>
									<Box style={{ marginTop: '10px' }}>
										<Button component="a" href={"/beer/" + beer.Slug} endIcon={<LaunchIcon/>} variant="contained" style={{ color: theme.palette.primary.contrastText, '&:hover': {color: theme.palette.primary.contrastText} }}>View this Beer</Button>
									</Box>
								</CardActions>
							</Card>
						))}
					</Masonry>
					</Box>
					</Box>}
					
					<Button
						onClick={() => {window.location.hash = 'beers'}}
						variant="contained"
						style={{ marginTop: '20px' }}
					>
						Go to Beers
					</Button>
					<div id="breweries"></div>
					<Typography variant="h3" style={{ marginTop: '50px', marginBottom: '20px', color: theme.palette.primary.dark }}>The Breweries {this.isAdmin() && <Button variant="outlined" onClick={() => {window.location = '/edit/brewery/0'}}><AddIcon/></Button>}</Typography>
					<Alert severity="info">Not all beers listed here are tapped right now. Check the beer list for all available beers</Alert>
					<Box style={{ marginTop: '20px', marginRight: '-16px' }}>
        	<Masonry columns={{xs: 1, md: 2}} spacing={2}>
						{this.state.breweries.map((brewery) => (
							<Card key={brewery.id} elevation={3}>
								<CardContent>
									<Typography variant="h4">
										{brewery.Name} {this.isAdmin() && <Button variant="outlined" onClick={() => {window.location = '/edit/brewery/' + brewery.id}}><EditIcon/></Button>}
									</Typography>
									{!brewery.isTappedByBrewery && <Alert severity="info" style={{ marginTop: '10px', marginBottom: '10px' }}>This brewery's beers are tapped by the ExBeerience staff, not the brewery itself</Alert>}
									<Box style={{ textAlign: 'center' }}>
										<img src={"logos/" + brewery.Logo} style={{ height: '100px', maxWidth: '100%', marginTop: '20px', marginBottom: '20px' }}/>
									</Box>
									<Typography
										style={{ whiteSpace: 'pre-wrap' }}
									>
										{brewery.Description}
									</Typography>
									<Typography variant="h4" style={{ marginTop: '20px', marginBottom: '10px' }}>
										{brewery.Name} Beers
									</Typography>
									{brewery.beers.map((beer) => (
										<Accordion key={beer.id}>
											<AccordionSummary
												expandIcon={<ExpandMoreIcon />}
												aria-controls="panel1a-content"
												id="panel1a-header"
											>
												<Typography variant="h5">{beer.Name}</Typography>
												<Typography variant="h6" style={{ paddingLeft: '20px', color: theme.text.secondary }}>{beer.Type}</Typography>
												<Typography variant="h6" style={{ paddingLeft: '20px', color: theme.text.secondary }}>{beer.Alc}% vol</Typography>
											</AccordionSummary>
											<AccordionDetails>
												<Typography>
													{beer.Description}
												</Typography>
												<Box style={{ marginTop: '10px' }}>
													<Button component="a" href={"/beer/" + beer.Slug} endIcon={<LaunchIcon/>} variant="contained" style={{ color: theme.palette.primary.contrastText, '&:hover': {color: theme.palette.primary.contrastText} }}>View this Beer</Button>
												</Box>
											</AccordionDetails>
										</Accordion>
									))}
								</CardContent>
								<CardActions>
									<Button component="a" href={brewery.Homepage} endIcon={<LaunchIcon/>} variant="contained" style={{ color: theme.palette.primary.contrastText, '&:hover': {color: theme.palette.primary.contrastText} }}>Homepage</Button>
								</CardActions>
							</Card>
						))}
          </Masonry>
					</Box>
				</Container>
			</ThemeProvider>
		);
	}
}

const container = document.getElementById('main');
const root = ReactDOM.createRoot(container);
root.render(<Main/>);
