<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class LoginController extends AbstractController
{
    #[Route('/login', name: 'login')]
    public function index(): Response
    {
        return $this->render('login/index.html.twig');
    }

    #[Route('/api/register', name: 'register_api')]
    public function register_api(Request $request, UserRepository $ur, UserPasswordHasherInterface $pwhi, ManagerRegistry $doctrine): Response
    {
        $data = $request->getContent();
        $data = json_decode($data);
        
        $em = $doctrine->getManager();
        $existingUser = $ur->findOneByUsername($data->username);
        if($existingUser != null) {
          return $this->json([
						'status' => 'error',
						'message' => 'this username has already been taken'
					]);
        }
        $user = new User();
        $user->setUsername($data->username);
        $user->setRoles(["ROLE_USER"]);
				$user->setFirstName($data->first_name);
				$user->setLastName($data->last_name);
        $hashedPassword = $pwhi->hashPassword(
            $user,
            $data->password
        );
        $user->setPassword($hashedPassword);
        $em->persist($user);
        $em->flush();
				return $this->json([
					'status' => 'OK',
					'message' => 'Registration Successful'
				]);
    }

    #[Route('/api/login', name: 'login_api')]
		public function login_api(Request $request): Response
		{
			return $this->json([
				'status' => 'OK',
				'message' => 'successfully logged in',
			]);
		}

    #[Route(path: '/logout', name: 'logout')]
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

		#[Route('/api/change-password', name: 'change_password_api')]
		public function change_password(Request $request, UserRepository $ur, UserPasswordHasherInterface $pwhi, ManagerRegistry $doctrine)
		{
				$data = $request->getContent();
				$data = json_decode($data);
				
				$em = $doctrine->getManager();
				$user = $this->getUser();
				$hashedPassword = $pwhi->hashPassword(
						$user,
						$data->password
				);
				$user->setPassword($hashedPassword);
				$em->persist($user);
				$em->flush();
				return new Response('OK');
		}
}
